import re
import sys


def lex(source, token_exprs):
    pos = 0
    tokens = []
    end = len(source)
    pr = []
    while pos < end:
        match = None
        for token_expr in token_exprs:
            pattern, tag = token_expr
            regex = re.compile(pattern)
            match = regex.match(source, pos)
            pr.append([pattern, tag, match])
            if match:
                text = match.group(0)
                if tag:
                    token = (text, tag)
                    tokens.append(token)
                break
        if not match:

            from vpi import log
            log('Illegal character {s}'.format(s=source[pos]))
            sys.exit(1)
        else:
            pos = match.end(0)
    # print("Here")
    # print([x for x in pr if print(x)])
    return tokens
