import lexer

RESERVED = 'RESERVED'
INT      = 'INT'
ID       = 'ID'

token_exprs = [
    (r'[ \n\t]+', None),
    (r'#[^\n]*',  None),
    (r'\:=',      RESERVED),
    (r'\(',       RESERVED),
    (r'\)',       RESERVED),
    (r';',        RESERVED),
    (r'[\+\-]?[0-9]+([\.,][0-9]+)?',   INT),
    (r'\+',       RESERVED),
    (r'\-',       RESERVED),
    (r'\*',       RESERVED),
    (r'/',        RESERVED),
    (r'<=',       RESERVED),
    (r'<',        RESERVED),
    (r'>=',       RESERVED),
    (r'>',        RESERVED),
    (r'=',        RESERVED),
    (r'!=',       RESERVED),
    (r'and',      RESERVED),
    (r'or',       RESERVED),
    (r'not',      RESERVED),
    (r'if',       RESERVED),
    (r'then',     RESERVED),
    (r'else',     RESERVED),
    (r'while',    RESERVED),
    (r'do',       RESERVED),
    (r'end',      RESERVED),
    (r'exponent',      RESERVED),
    (r'[A-Za-z][A-Za-z0-9_]*', ID)
]


def vpi_lex(source):
    return lexer.lex(source, token_exprs)


# from vpi import  log
# log("Lexer is loaded")