import os
import sys
import subprocess
from GUI_ui import *
from PyQt5 import QtCore, QtGui, QtWidgets


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.Open_BTN.clicked.connect(self.opeFunction)
        self.ui.Run_BTN.clicked.connect(self.runFunction)
        self.ui.Exit_BTN.clicked.connect(self.exitFunction)

    def opeFunction(self):
        path = os.path.join(os.getcwd(), 'Examples')
        if os.path.exists(path):
            pass
        else: path = os.getcwd()
        file = QtWidgets.QFileDialog.getOpenFileName(self, 'Select Source Code File', path)
        if file[0] != '':
            with open(str(file[0]), "rt") as f:
                temp = f.read()
            f.close()
            self.ui.Code_TE.setText(temp)
        else: pass

    def runFunction(self):
        temp = self.ui.Code_TE.toPlainText() or ""
        with open('temp.vpi', 'wt') as f:
            f.write(temp)
        f.close()
        cmd = [os.path.join(os.getcwd(), 'vpi.py')]
        cmd += ['-e', 'temp.vpi']
        p = subprocess.Popen(["python3"] + cmd, stdout=subprocess.PIPE)
        # stdout = subprocess.PIPE - означает, что вывод программы перехватывается
        p.wait()
        Result = p.communicate()[0]  # получаем перехваченный вывод
        Result = str(Result, "UTF-8")
        self.ui.Console_TE.setText(Result)

    def exitFunction(self):
        exit()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = MyWin()
    myapp.show()
    sys.exit(app.exec_())
