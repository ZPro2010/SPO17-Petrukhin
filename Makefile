
all: run

build: Dockerfile src/*
	docker build -t petrukhin-spo17 .
	touch build

run: build
	docker run \
		--rm \
		--net host \
		-v ${HOME}/.Xauthority\:/root/.Xauthority \
		-v /tmp/.X11-unix\:/tmp/.X11-unix \
		-e DISPLAY\=unix${DISPLAY} \
		petrukhin-spo17

clean:
	rm build
	docker rmi petrukhin-spo17

