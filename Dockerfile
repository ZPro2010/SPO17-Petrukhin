FROM debian

RUN apt update && apt install -y python3-pip
RUN apt install -y pyqt5-dev-tools
RUN pip3 install click
ADD src /app
WORKDIR /app
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
CMD python3 /app/main.py
