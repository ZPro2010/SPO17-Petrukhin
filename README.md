Very Primitive Interpreter in Python.  
GUI created with PyQt5.  
All project packed into Docker.  
To start just prompt into terminal: "make run"  
Tested in Gentoo Linux. Worked fine.  

docker info   

Images: 116  
Server Version: 17.06.0-dev  
Storage Driver: btrfs  
 Build Version: Btrfs v4.9  
 Library Version: 102  
Logging Driver: json-file  
Cgroup Driver: cgroupfs  